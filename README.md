<p align="center">
	<a href="https://maimengcloud.com"  target="_blank">
	    <img src="./docs/images/index/mm_logo_big.png" width="400" alt="logo">
	</a>
</p>
<p align="center">
	<strong>唛盟(mdp-lcode vue3 )：多功能、高效率、低代码(支持0代码) 前后端一体化、智能化的开发工具</strong>
</p>

<p align="center">
	<a target="_blank" href="https://gitee.com/maimengcloud/mdp-lcode-ui-web">
        <img src='https://gitee.com/maimengcloud/mdp-lcode-ui-web/badge/star.svg?theme=gvp' alt='gitee star'/>
    </a> 
</p>
<p align="center">
	👉 <a target="_blank" href="https://maimengcloud.com/lcode">https://maimengcloud.com </a>  👈
</p>



## 快速导航  

- [唛盟-后端开发底座](https://gitee.com/maimengcloud/mdp-core)   
- [前端组件](https://gitee.com/maimengcloud/mdp-lcode-ui-web)   
- [后端服务](https://gitee.com/maimengcloud/mdp-lcode-backend)    
- [体验环境](https://maimengcloud.com/lcode/)   
  登陆界面上选择演示账号登陆或者直接扫码登陆，无须注册   
- 👉[教程]b站上搜素 [唛盟9哥教你撸前后端代码](https://www.bilibili.com/video/BV111421S72r/?spm_id_from=333.337.search-card.all.click&vd_source=93be23d03863773d50b81112985b9237)


## 📢 简介[唛盟低代码开发平台mdp-lcode](/)

唛盟低代码开发平台简称唛盟或者mdp. 👉[唛盟-总体介绍](https://www.bilibili.com/video/BV111421S72r/?spm_id_from=333.337.search-card.all.click&vd_source=93be23d03863773d50b81112985b9237)
唛盟旨在为企业开发业务系统提供一整套解决方案，唛盟具有高效率、低代码、支持0代码、功能丰富等特点。企业可以在唛盟工程之上，加入更多其它业务功能；也可以以唛盟作为模板，创建新的工程，用于开发其它业务。使用唛盟构建应用，您不用考虑多租户、登录、统一认证中心、权限、菜单管理、系统管理、公共组件、公共api、代码冗余、数据字典、图片库、文件库、智能表单、工作流、微服务互相调用、全局跟踪定位bug、多主键crud,复杂sql查询等各种问题，这些问题的解决方案都作为扩展功能内置唛盟工程中了。  
💪给你一个使用唛盟的理由：代码大量减少、开发so easy、前后端MIT协议、全部开源、永久免费  

## 📢 唛盟生态

💪唛盟生态遵循 <strong>“一个底座+N个专业子系统”</strong> 的架构，基于同一个底座的各个专业子系统可以任意组合形成一个大的业务系统。👉[聊聊唛盟生态1+n架构](https://www.bilibili.com/video/BV1BD421V7Bu/?spm_id_from=333.337.search-card.all.click&vd_source=93be23d03863773d50b81112985b9237)  

### 底座 mdp-core 
  [mdp-core](https://gitee.com/maimengcloud/mdp-core)   
### N个专业子系统
| 唛盟子系统                        | 说明               | 版本     |
|----------------------------------------------------------------------|------------------|--------|
| [低代码、0代码](https://gitee.com/maimengcloud/mdp-lcode-ui-web)| 低代码、0代码框架           | 3.0.0 |
| [账户、权限、组织管理](https://gitee.com/maimengcloud/mdp-lcode-backend/tree/master/mdp-sys)| 账户、权限、组织管理，支持多租户 | 3.0.0  |
| [数据模型](https://gitee.com/maimengcloud/mdp-dm-backend/tree/master/mdp-dm-backend)|表结构设计、表数据增删改查等ddl、dml操作，在线执行sql等  | 3.0.0 |
| [第三方支付登录等](https://gitee.com/maimengcloud/mdp-tpa-backend)  | 微信支付、支付宝支付、paypal支付、第三方登录   | 3.0.0  |
| [统一认证中心](https://gitee.com/maimengcloud/mdp-oauth2-backend) | 短信、微信、账号登录 | 3.0.0 |
| [统一流程中心](https://gitee.com/maimengcloud/mdp-workflow-backend)| 审批、代办、设计流程、监控流程等  | 3.0.0 |
| [短信](https://gitee.com/maimengcloud/mdp-sms-ui-web) | 群发消息              | 3.0.0  |
| [代码生成器](https://gitee.com/maimengcloud/mdp-code-generator)| 自动生成前后端代码          | 3.0.0  |
| [研发项目管理](https://gitee.com/maimengcloud/xm-ui-web)| 产品管理、需求管理、任务计划、迭代、测试、效能等         | 3.0.0  |
| [即时通讯](https://gitee.com/maimengcloud/mdp-im-web)  | 即时通讯、消息、聊天            | 3.0.0  |
| [财务](https://gitee.com/maimengcloud/ac-core-ui-web)| 财务子系统，凭证、报销、会计记账、成本、结算 等          | 3.0.0 |
| [协同办公](https://gitee.com/maimengcloud/oa-ui-web)  | 办公用品、会议、车辆、资产、档案、用印、采购、绩效等功能           | 3.0.0 |

## ⚙ 技术栈
1. 前端  

| 框架                                                                   | 说明               | 版本     |
|----------------------------------------------------------------------|------------------|--------|
| [Vue](https://staging-cn.vuejs.org/)                                 | Vue 框架           | 3.3.8 |
| [Vite](https://cn.vitejs.dev//)                                      | 开发与构建工具          | 4.5.0  |
| [Element Plus](https://element-plus.org/zh-CN/)                      | Element Plus     | 2.4.2 |
| [TypeScript](https://www.typescriptlang.org/docs/)                   | JavaScript 的超集   | 5.2.2  |
| [pinia](https://pinia.vuejs.org/)                                    | Vue 存储库 替代 vuex5 | 2.1.7 |
| [vueuse](https://vueuse.org/)                                        | 常用工具集            | 10.6.1 |
| [vue-i18n](https://kazupon.github.io/vue-i18n/zh/introduction.html/) | 国际化              | 9.6.5  |
| [vue-router](https://router.vuejs.org/)                              | Vue 路由           | 4.2.5  |
| [unocss](https://uno.antfu.me/)                                      | 原子 css          | 0.57.4  |
| [iconify](https://icon-sets.iconify.design/)                         | 在线图标库            | 3.1.1  |
| [wangeditor](https://www.wangeditor.com/)                            | 富文本编辑器           | 5.1.23 |
| [form-create](https://gitee.com/xaboy/form-create/) | 表单引擎+表单设计器           | vue3版 |

2. 后端  

| 框架                                                                   | 说明               | 版本     |
|----------------------------------------------------------------------|------------------|--------|
| spring boot                               | spring boot 框架           | 2.4.1 |
| mybatis plus                                      | 数据库操作框架          | 3.5.3.1  |
| spring security                                   | 安全框架 | 2.1.7 |
| jsqlparse                                     | sql解析引擎            | 4.7+ |
| swagger                                     | 接口说明框架            | 2.2.8 |
| logback                                     | 日志框架            | 1.2.3 |
| jexl13                                     | 表达式引擎            | 3.1|
| flowable                      | 流程引擎-可换     | 6.4.2 |
| spring cloud                                     | cloud框架-可换            | 2020.0.0 |
| spring cloud consul                                    | cloud框架-可换            | 1.10+ |
| spring cloud consul                                    | cloud框架-可换            | 1.10+ |
| spring oauth2                   | 统一认证中心-可换   | 5.2.2  |

## ⚙ 开发工具

前端  
推荐 VS Code 开发，配合插件如下：  

| 插件名                           | 功能                       |
|-------------------------------|--------------------------|
| node.js                       | node.js   建议  21.1.0 +             |
| pnpm                       | 类似npm的构建工具    最新版即可            |
| nvm                      | node版本管理工具，多版本之间切换很好用 最新版即可                |
| TypeScript Vue Plugin (Volar) | 用于 TypeScript 的 Vue 插件  |
| Vue Language Features (Volar) | Vue3.0 语法支持              |
| unocss                        | unocss for vscode           |
| Iconify IntelliSense          | Iconify 预览和搜索           |
| i18n Ally                     | 国际化智能提示               |
| Stylelint                     | Css    格式化               |
| Prettier                      | 代码格式化                   |
| ESLint                        | 脚本代码检查                  |
| DotENV                        | env 文件高亮                 |
| [ruoyi-vue-pro](https://gitee.com/zhijiantianya/ruoyi-vue-pro)|前端流程ui基于若依修改



后端  
| 插件名                           | 功能                       |
|-------------------------------|--------------------------|
| idea                        | java 开发工具    社区版、企业版都可            |


## 😭 日常开发中，您是否有以下痛点？

- **团队中缺乏企业级前后端分离的开发底座**，需要在各种框架中进行摸索、整合。 
- ai时代，开发工具如何去支持未来的软件开发？
- 重复造轮子现象严重、浪费人力、对开发者经验要求过高。
- 缺乏统一的开发模式，缺乏公共组件的抽取和共享机制，导致业务代码混乱不堪、代码臃肿、bug多、维护困难
- 缺乏统一的足够灵活的权限管理机制，开发人员不得不写一堆的权限代码混入业务代码中，前端权限、后端权限控制混乱不堪
- 缺乏统一的能够覆盖前后端的、满足前后端分离的代码生成器，代码模板无法按企业现状进行重新编辑、修改
- 缺乏统一的编程规范，或者具有书面编程规范，难以贯彻落实到开发中，代码还是五花八门
- 缺乏统一的元数据(数据字典)管理机制，前后的数据共享调用困难，下拉列表数据混乱不堪
- 缺乏统一的流程管理机制，要想进行流程类业务开发非常困难
- 缺乏统一的国际化机制，国际化实施困难，不得不针对各种语言发布多个版本，无法解决后端国际化、前端国际化等问题
- 缺乏统一的微服务、分布式系统整合机制，微服务互相调用、微服务的权限管理困难
- 缺乏统一的认证中心，单点登录实施困难
- 缺乏统一的支付整合机制，接入微信、支付宝、paypal等困难
- 缺乏项目管理工具，项目计划、任务委派、质量管理、需求管理、持续集成等完全没概念
- **让 唛盟-mdp 来帮你解决这些痛点吧！然而，这些只是 唛盟-mdp 解决的最基础的功能。**

## 😁 为什么要使用唛盟产品

- 唛盟所有子系统从前端到后端提供ai支持能力
  1. 前端每个字段可以调起ai指令、el表达式
  2. 后端每个接口可以支持识别ai指令、el表达式
  3. 后端支持接入大模型
  4. 具有根据企业数字资产进行ai自训练能力

- 完全开源、永久免费的企业级开发底座
  1. 使用mdp能够带来开发效率的大幅提升，代码行数大幅减少，质量提升明显
  2. 使用mdp能够大幅度降低对开发人员的经验要求，大幅度降低人力成本
  3. mdp对各种开源组件进行了融合改进，提供了针对企业开发中各种问题的最佳解决方案
  4. 企业使用一套开源软件即同时拥有前端开发框架及后端开发框架

- 统一的开发模式
  1. 前后端分离
  2. 前后端都分别进行了技术组件、业务组件的抽取、共享，企业可以进行再提炼、抽象，形成更多的公共组件，对后续开发形成强力的支撑作用

- 足够灵活的权限管理机制
  1. 前端提供统一的按钮级别的权限判断接口、提供路由菜单的权限控制机制
  2. 后端实现api接口的自动注册、自动审核
  3. 基于岗位-部门-角色-菜单及按钮-后端api-人员 6要素的权限管理机制，可以0编程实现绝大多数的权限需求

- 基于领域驱动设计(DDD)的框架及代码生成器
  1. 代码生成器覆盖前端、后端，支持任意时刻的重新生成，支持命令行、开发工具插件、在线三种方式生成代码，生成的代码可以0编程使用
  2. 代码生成器代码模板可以按企业现状进行修改、满足不断发展、持续改进的需求
  3. 支持多个表一次性生成，也就是可以一次性生成几十到几百张表的增删改查功能，而开发人员仅需要填写表名即可完成

- 提供完整的编程规范说明
  1. mdp的框架提供了完备的接口说明、组件说明、组件使用场景等
  2. mdp维护团队提供在线支持，及时解答、解决开发者使用过程中的问题

- 提供强大的元数据(数据字典)管理机制
  1. 内置了元数据管理模块，并实现了元数据的分布式缓存、客户端缓存、元数据分发、缓存清理等
  2. 开发者在客户端、任意微服务中、任意单体应用中可以快速获取元数据
  3. 元数据的调用效率等同于调用本地map缓存，几乎可以忽略使用元数据的性能开销问题

- 整合了最新版本的强大的flowable工作流引擎
  1. 基于mdp框架重新开发了流程中心、任务中心、流程的发布、上下架等功能
  2. 提供分布式环境下的流程调用、流程整合问题的解决方案
  3. 提供vue3版本在线流程设计器，并整合了mdp的权限机制
  4. 整合了mdp的智能表单
  5. 整合了mdp的数据字典，通过配置即可实现大部分业务场景，真正做到流程设计、部署、运行0耗时，0延迟。
  4. 任务的提交支持几乎能想到的各种方式：转办、委办、主办、前加签、后加签、减签、发起人处理、驳回任意节点、审批同意、审批不同意但流程继续、拒绝终止流程等，同时提供框架的提交方式可扩展性，让你轻松应对各种变态需求。
  5. 会签支持：会签、或签、过半通过、一票否决等等
  6. 任务分配到人十来种策略支持并提供扩展接口：按岗位、按部门、按组、按标签、按指定人员、发起人、上一步执行人、上一步执行人临时指定、发起流程时统一指定、转主办、转监控、按高级自定义查询条件、
  7. 整合了mdp的消息发送机制，轻松发送流程消息到im、站内信、公众号、短信
  8. 整合mdp事件模型，为将来ai调用流程等创新场景提供底层基础
  9. 提供业务模块内嵌页面发起流程的组件和统一的方式，开发几乎可以无视工作流相关的接入工时。
  10. 整合mdp附件管理


- 提供强大的国际化解决方案
  1. 前后端均支持分别进行国际化
  2. 多语言的支持与业务代码完全解耦，彻底解决硬编码进行语言切换的问题

- 整合了强大的微服务框架
  1. mdp平台任意组件均同时支持微服务环境、单体应用环境运行，开发人员开发的时候可以以单体应用的方式开发，然后以微服务方式发布到生产、测试环境
  2. 提供微服务的治理

- 强大的DAO层
  1. 支持基于xml文件的sql编写
  2. 支持无xml方式的数据访问
  3. 支持多主键（对mybatis plus进行升级，解决了多主键、多表联合查询等问题）
  4. 支持多数据源，通过备注实现数据源切换
  5. 支持前端构建任意复杂的查询条件并提供对应的最佳实践，支持前端输入框输入>,=,*,$IS NULL,$IN,$NOT IN等运算操作符，支持前端通过 or and 连接符构建任意复杂的条件表达式

- 强大的web ui
  1. 提供好用好看的ui组件库
  2. 提供页面高级查询功能、可以组装任意复杂的查询条件
  3. 提供针对元数据(数据字典)的引用、针对任意表的引用的组件库
  4. 提供导入、导出等基础功能
  5. 提供按钮权限判断接口、
  6. 提供动态菜单功能

- 提供自定义表单解决方案，具有0代码0延时发布一个新功能的能力
  1. 自定义表单设计
  2. 表单展现
  3. 表单数据管理
  4. 自定义表单与工作流整合使用
  5. 自定义表单发布成普通菜单

- 整合了微信支付、支付宝支付、paypal支付
  1. 提供支付、订单、支付通知底层框架，可以快速整合各种支付功能
  2. 整合了微信支付功能，进行配置文件更新即可使用
  3. 整合了支付宝支付功能，进行配置文件更新即可使用
  4. 整合了paypal支付，进行配置文件更新即可使用

- 整合了oauth2.0框架
  1. 提供oauth2.0的整体框架，构建统一认证中心、单点登录等不再是难事
  2. 整合了微信、支付宝、手机验证码、账户密码等登录方式

## 💪 内置功能、界面展示

### 组织管理
|功能                        |描述|
|-------------------------------|--------------------------|
|机构管理                  | 企业信息维护、管理员维护、账户信息维护、企业产品维护、|
|用户管理                  | 用户的crud、分配岗位、分配部门、重置密码、邀请|
|部门管理                  | 部门curd、分配岗位到部门、查看部门用户|
|公司管理                  | 公司curd（超级管理员使用）|
|岗位管理                  | 岗位curd、分配角色到岗位、岗位人员查看|
|注销审核                  | 审核用户的注销申请|

![branch-set](./docs/images/module/branch-set.png)

![dept-set](./docs/images/module/dept-set.png)


### 个人中心  
|功能                      |描述|
|-------------------------------|--------------------------|
|个人信息                | 基本信息、邮箱、手机、企业绑定、微信绑定等|
|我的消息                | 接收个人私信、公告等|
|登录日志                | 查询我的登录日志 |

![user](./docs/images/module/profile-base.png)
### 角色权限
|功能                        |描述|
|-------------------------------|--------------------------|
|角色管理                  | 角色crud、分配菜单、分配权限|
|权限定义                  | 权限crud|
|菜单管理                  | 菜单(按钮)crud、分配菜单(按钮)给角色、|
|模块管理                  | 模块crud|
|已开模块                  | 查看企业(个人)已开通的模块|

![post-set](./docs/images/module/post-set.png)
![role](./docs/images/module/role.png)

### 平台管理
|功能                        |描述|
|-------------------------------|--------------------------|
|平台配置                  | 配置平台的信息|
|个人认证审核               | 审核个人的实名认证请求|
|企业认证审核               | 审核企业的实名认证请求|

![platform](./docs/images/module/platform.png)

### 元数据管理
|功能                        |描述|
|-------------------------------|--------------------------|
|字典管理                  | 数据字典的crud|
|列表维护                  | 下拉列表数据项的crud|
|参数定义                  | 公共系统参数的crud|


![metaItem](./docs/images/module/metaItem.png)
![itemSet](./docs/images/module/itemSet.png)
![optionList](./docs/images/module/optionList.png)

### 数据模型
|功能                        |描述|
|-------------------------------|--------------------------|
|模型中心                  | 模型的crud|
|表格结构                  | 表结构预览、表结构修改、表的字段维护等|
|表格数据                  | 表的业务数据crud等|
|数据集市                  | 可以快速构造查询sql，提供给外部接口、智能表单、数据分析等使用|
|创建数据集                  | 数据集的设计、保存、预览数据等|

![dm-table-struct](./docs/images/module/dm-table-struct.png)
![dm-table-data](./docs/images/module/dm-table-data.png)
![dm-sql-exec](./docs/images/module/dm-sql-exec.png) 

### 智能表单
支持mdp-ui组件的拖拉拽，支持表单0编码0耗时发布形成一个新的功能  

|功能                        |描述|
|-------------------------------|--------------------------|
|表单中心                  |表单列表，权限管理，数据查阅等 |
|创建表单                  | 表单设计、预览|

![formDesigner](./docs/images/module/form-designer.png)

### 内容管理
|功能                        |描述|
|-------------------------------|--------------------------|
|附件库                  | 附件上传、预览、权限配置等|
|图片库                  | 图片上传、预览、删除等|
|图标库                  | 图标的预览、选用等|

![images](./docs/images/module/images-list.png)

![icon](./docs/images/module/icon-list.png)

### 订单管理
|功能                        |描述|
|-------------------------------|--------------------------|
|我的订单                  | 订单查看、管理、打折促销等|
|购买产品                  | 下单购买应用|
|增购人数                  | 增加企业人数|
|续费                     | 到期续费|

![order](./docs/images/module/mall-order.png)  

### 第三方管理 
|功能                        |描述|
|-------------------------------|--------------------------|
|邀请管理                  | 查询、管理我发起的邀请|
|我邀请的用户              | 查询我邀请的用户列表|
|第三方用户查询            | 查询第三方账户信息 |

 
### 流程模型
|功能                        |描述|
|-------------------------------|--------------------------|
|流程编辑器                | 在线流程编辑器|
|模型发布                  | 流程模型发布|
|模型设置                  | 设置流程与智能表单的关联、模型的启动条件、审批人、权限等|

![modelflow](./docs/images/module/modelflow.png)  
![workflow-model-designer](./docs/images/module/workflow-model-designer.png)

![workflow-model-assignee](./docs/images/module/workflow-task-assignee.png)
![workflow-model-form](./docs/images/module/workflow-task-form.png)

### 审批中心
|功能                        |描述|
|-------------------------------|--------------------------|
|待执行                    | 我的待执行任务查询、任务执行|
|日历任务                  | 我的待执行任务查询、任务执行|
|发起流程                  | 发起流程|


![workflow-task-detail](./docs/images/module/workflow-task-detail.png)
![workflow-task-action](./docs/images/module/workflow-task-action.png)
![workflow-task-ruli](./docs/images/module/workflow-task-ruli.png)
![workflow-task-start](./docs/images/module/workflow-task-start.png)

### 审批监控
|功能                        |描述|
|-------------------------------|--------------------------|
|我发起的流程                  | 查询我发起的流程|
|我监控的流程                  | 查询我监控的流程、催办|
|我参与的流程                  | 查询我参与过的流程|

![monitorflow](./docs/images/module/monitorflow.png)

### 低代码 
|功能                        |描述|
|-------------------------------|--------------------------|
|代码生成器                 | 在线生成crud的前端页面代码(表单、表格、api)，后端代码（控制层、服务层、dao层代码），前后端0修改即可正常运行 |

### 界面风格切换

![black-style](./docs/images/module/black-style.png)

![black-branch-set](./docs/images/module/black-branch-set.png)

![blackdept](./docs/images/module/blackdept.png)

## ⚙ 组件库[mdp-ui](/src/components/mdp-ui)  

mdp封装了几十个ui组件,使用mdp-ui与不使用mdp-ui的区别：写100行代码还是写1行代码的区别，代码量的减少立竿见影
- [mdp-ui](/src/components/mdp-ui)  
mdp的核心组件，该组件库为纯前端页面组件，无须配置任何后端api接口即可正确运行
- [mdp-ui-ext](/src/components/mdp-ui-ext)  
基于mdp-ui扩展的带有对接后台api业务加载能力的业务组件

>⚠️注意：先行体验一把[mdp-ui组件效果预览](https://maimengcloud.com/lcode/#/mdp/lcode/index)

### 非唛盟的子系统如何快速整合mdp-ui组件库
>找到[main.ts](/src/main.ts) 添加下面代码
```js

//mdp-ui 基础组件
import MdpComponents from '@/components/mdp-ui/index.js'
//mdp-ui-ext拓展的组件
import MdpUiExtComponents from '@/components/mdp-ui-ext/index.js'
//唛盟公共api
import MdpPubApi from "@/api/mdp_pub";  

const app = createApp(App)

app.use(MdpComponents) 
app.use(MdpUiExtComponents)    
app.config.globalProperties.$mdp=MdpPubApi 

```

### [mdp-dialog](/src/components/mdp-ui/mdp-dialog)  
弹框，可以把任意页面装配成弹框，无须定义多余的变量及函数  
- 相对于el-dialog来说，要使用弹窗，事情还真不少。需要定义visible变量，定义showDialog,closeDialog两个函数，最关键一点是如果要父页面传递参数到窗口内组件，还得在父页面定义一个变量，通过props属性传递。相当麻烦。如果某个功能弹窗特别多，会有一堆的控制弹窗的变量及函数，非常恶心。mdp-dialog则完全解决上述问题。

### [mdp-table](/src/components/mdp-ui/mdp-table)  
表格，内置了增、删、改、查、高级查询、重置查询、导出、列配置、分页、批量编辑等功能、内置了对按钮权限的控制机制  
功能非常强大，懂的都懂。

- 综合管理  
![综合管理表格](/docs/images/ui-components/table-mng.jpg)  
- 可编辑表格    
![可编辑表格](/docs/images/ui-components/edit-table.jpg)  
- 可编辑-树状表格    
![可编辑-树状表格](/docs/images/ui-components/left-right-tree-table.jpg)  

### [mdp-select](/src/components/mdp-ui/mdp-select)  
下拉列表，支持对数据字典、元数据的引用，支持对任意小表表格数据的引用，支持参数化加载后台数据，对后台加载的数据进行缓存   
![mdp-select](/docs/images/ui-components/select-origin.jpg)  

### [mdp-select-table](/src/components/mdp-ui/mdp-select-table)  
超大表格下拉列表，与mdp-select相比，该组件具有分页查询功能   
![分页选择表格数据](/docs/images/ui-components/big-table-select.jpg)  

### [mdp-select-user](/src/components/mdp-ui-ext/mdp-select-user)  
用户选择下拉列表，与mdp-select-table组件类似，仅仅针对用户的头像做了特殊处理  
![用户选择](/docs/images/ui-components/select-user.jpg)  

### [mdp-input](/src/components/mdp-ui/mdp-input)  
输入框

### [mdp-date](/src/components/mdp-ui/mdp-date)  
日期

### [mdp-date-range](/src/components/mdp-ui/mdp-date-range)  
区间日期

### [mdp-number](/src/components/mdp-ui/mdp-number)  
数字输入

### [mdp-hi-query](/src/components/mdp-ui/mdp-hi-query)  
高级查询，可以由用户自定义任意复杂的查询条件  
![高级查询](/docs/images/ui-components/hi-query.jpg)  

### [mdp-table-configs](/src/components/mdp-ui/mdp-table-configs)  
表格配置，用于控制表格的列显示与否  
![表格配置列显示隐藏](/docs/images/ui-components/table-column-configs.jpg)  

### [mdp-table-kanban](/src/components/mdp-ui/mdp-table-kanban)  
看板 多维度洞察数据
![看板多维度洞察数据](/docs/images/ui-components/mdp-table-kanban/mdp-table-kanban.png)  


### [mdp-tree](/src/components/mdp-ui/mdp-tree)  
懒加载树，自由拖拽，调整上下级关系手到擒来
![懒加载树，自由拖拽，调整上下级关系手到擒来](/docs/images/ui-components/mdp-tree/mdp-tree-drag.png)  


### [mdp-lr-box](/src/components/mdp-ui/mdp-lr-box)  
分栏布局，比起el-row，更优秀更简洁，更灵活
![分栏布局，比起el](/docs/images/ui-components/mdp-lr-box/mdp-lr-box-dept-tree.png)  

### [mdp-transfer](/src/components/mdp-ui/mdp-transfer)  
穿梭框

### [mdp-cate-tree](/src/components/mdp-ui-ext/mdp-cate-tree)  
文档、文章目录树

### [mdp-expand](/src/components/mdp-ui-ext/mdp-expand)  
智能表单相关组件

### [mdp-ext-infos](/src/components/mdp-ui-ext/mdp-ext-infos)  
自动扩展字段信息


### [mdp-image](/src/components/mdp-ui-ext/mdp-image)  
图片库，图片的增删改查、上传、下载

### [mdp-select-image](/src/components/mdp-ui-ext/mdp-select-image)  
图片选择器,支持图片的上传下载、上传后的统一管理、共享

### [mdp-select-dept](/src/components/mdp-ui-ext/mdp-select-dept)  
部门选择，支持树状机构，异步加载，支持分页查询，任意大数据量

### [mdp-select-att](/src/components/mdp-ui-ext/mdp-select-att)  
附件库,支持附件的上传下载、上传后的统一管理、共享

### [mdp-select-cate](/src/components/mdp-ui-ext/mdp-select-cate)  
文档、文章目录树选择器

### [mdp-select-tag](/src/components/mdp-ui-ext/mdp-select-tag)  
标签库,支持标签的统一管理、共享


### [mdp-meta-item](/src/components/mdp-ui-ext/[mdp-meta-item)  
元数据管理库、支持元数据的crud

### [mdp-meta-option](/src/components/mdp-ui-ext/mdp-meta-option)  
元数据管理库-列表数据组件、支持列表数据的crud

### [mdp-rich-text](/src/components/mdp-ui-ext/mdp-rich-text)  
富文本编辑器，整合了mdp-select-image作为插件

## 🔃 api
- [api](/src/api/)  
业务api，哪里用到哪里就单独引入
### [api/mdp_pub/index.js](/src/api/mdp_pub/index.js)  
公共api总入口,注册到vue全局函数中，可以在页面中任意地方直接使用,公共api总入口，如需要添加公共api,请在此文件添加  
### [mdp_api_base.js](/src/api/mdp_pub/mdp_api_base.js)  
为mdp框架核心的与后端交互的api,自动注册为全局公共函数，可通过$mdp.xxx调用，可改，但不要新增，删除任何一个api.
### [mdp_api_ext.js](/src/api/mdp_pub/mdp_api_ext.js)  
为mdp扩展的组件与后端交互的api,自动注册为全局公共函数，可通过$mdp.xxx调用，可改，但不要新增，删除任何一个api.
### [mdp_api_biz.js](/src/api/mdp_pub/mdp_api_biz.js)  
为mdp扩展的组件与后端交互的api,自动注册为全局公共函数，可通过$mdp.xxx调用，可改,可新增必要的api.

### [mdp_config.js](/src/api/mdp_pub/mdp_config.js)
前端工程的一些公共配置项，主要配置每一类api对接哪个后端微服务
根据不同的环境，需要修改部分内容。mdp_config配置项已与vue3工程公共配置打通，vue3依赖于VIET_*的全局配置项，因此，vue3及更高版本之后应该以VITE的配置为准，详细查阅 [env.d.ts](/types/env.d.ts)

```js

# 固定网址
VITE_FIXED_DOMAIN=https://www.maimengcloud.com

# 开源项目地址
VITE_OPEN_SOURCE_LINK=https://gitee.com/qingqinkj218/collections/375320

# 微信开放平台 appid
VITE_WXOPEN_APPID=wx2671d5db8346b6fc

#
# 说明： VITE_CTX_# 为定义唛盟各个子系统的context;
# context指向子系统的微服务名称，spring.application.name
#  
#  1. 用于前端请求路径前缀映射，
#     前端页面一般部署在 ./{VITE_CONTEXT}/{VITE_UI_VERSION?}
#  
#  2. api导航到对应子系统等
#     api一般按以下格式规范组装 ./{VITE_API_BASE_PATH}/{VITE_API_VERSION}/{VITE_CONTEXT}
# 

# 登录相关，如果后端启用oauth2,则返回 oauth2client ，如果后端不启用oauth2,后端账户使用mdp-lcode服务，则返回 lcode ,如果使用mdp-sys提供服务，则返回sys
VITE_CTX_LOGIN=oauth2client

# 项目管理子系统
VITE_CTX_XM=xm

# 工作流子系统,由于flowable启动限制，目前不支持jar集成到lcode，需要单独部署
VITE_CTX_WF=workflow

# 低代码子系统
VITE_CTX_LCODE=lcode

# 内容管理子系统
VITE_CTX_ARC=arc

# 数据模型 由lcode接管
VITE_CTX_DM=lcode

# 协同办公
VITE_CTX_OA=oa

# 系统管理,目前由lcode接管
VITE_CTX_SYS=lcode

# 智能表单，目前由lcode接管
VITE_CTX_FORM=lcode

# 商城-面向消费用户端
VITE_CTX_MALL=mall

# 商城管理端
VITE_CTX_MALLM=mallm

# 即时通讯
VITE_CTX_IM=im

# 短信
VITE_CTX_SMS=sms

# 唛盟内置的应用购买模块订单 目前由lcode接管
VITE_CTX_MO=lcode

# 众包 目前由xm接管
VITE_CTX_CROWD=xm

# 第三方集成系统
VITE_CTX_TPA=tpa
```

 
#### api的调用方法
```html
<el-button @click="$mdp.xxxApi()"/>
```
```js
  //选项式
  this.$mdp.xxxApi()

  //组合式 由于没有this,需要在顶部引入proxy
  const {proxy}: any = getCurrentInstance()
  proxy.$mdp.xxxApi()
```

- [login](/src/api/login/index.ts.ts)  
登录相关api 

## 🔃 指令
- [v-adaptive](./src/components/mdp-ui/directive/adaptive.ts) 
让表格、div等自动适应，底部始终保持距离视窗底部一定距离(默认 30px)

```html
<el-table v-adaptive="{bottomOffset:30}"/> 

<el-table v-adaptive/>
```


## 💻 样例项目

- [低代码平台] (https://maimengcloud.com/lcode/)
- [系统管理] (https://maimengcloud.com/sys)
- [协同办公] (https://maimengcloud.com/oa)
- [项目管理] (https://maimengcloud.com/xm)
- [流程管理] (https://maimengcloud.com/wf)

## ⚙ 快速开始

>⚠️注意：该项目使用 element plus 版本，所以最低兼容 vue@3.0+ nodejs版本v21.1.0

>⚠️注意：强烈建议一定要用pnpm
```js
// 全局安装
npm install pnpm -g
 
//查看源
pnpm config get registry 
//切换淘宝源
pnpm config set registry https://registry.npmmirror.com/
 
// 使用
pnpm install 包 // 安装依赖

```

### 前序准备

- 当前项目属于网页版前端ui项目，后端代码请移步到 [mdp-lcode-backend](https://gitee.com/maimengcloud/mdp-lcode-backend)

- 代码生成器下载 [mdp-code-generator](https://gitee.com/maimengcloud/mdp-code-generator) 也可以在低代码平台在线生成

### 开发
```bash
# 克隆项目
git clone https://gitee.com/maimengcloud/mdp-lcode-ui-web.git

# 安装依赖
pnpm install 

# 启动服务
pnpm run dev
```
浏览器访问 http://localhost:8015

#### 登录相关
登录api，对接后台接口有两种方式:  
1. 对接统一认证中心  
```js
 VITE_CTX_LOGIN=oauth2client
```
2. 对接mdp-lcode-backend进行登录
```js
 VITE_CTX_LOGIN=lcode
```

#### 图片库、素材库、标签库
内容相关接口默认打包进mdp-lcode，所以内容相关接口可以直接对接lcode即可，如果内容相关的服务使用独立的第三方开源工程，独立部署，比如直接使用 [mdp-arc-backend](https://gitee.com/maimengcloud/mdp-arc-backend)  
1. 对接mdp-arc-backend
```js
   VITE_CTX_ARC=arc

   # 配置附件上传的接口，如果不配使用arc内置的地址
   VITE_UPLOAD_ATT_URL=

   # 配置图片上传的接口，如果不配使用arc内置的地址
   VITE_UPLOAD_IMAGE_URL=
   
```
2. 对接mdp-lcode-backend
```js
  VITE_CTX_ARC=lcode

  # 配置附件上传的接口，如果不配使用arc内置的地址
  VITE_UPLOAD_ATT_URL=

  # 配置图片上传的接口，如果不配使用arc内置的地址
  VITE_UPLOAD_IMAGE_URL=
```

#### 前后端对接
 
前后端对接，涉及api/lcode,api/arc,api/oauth2client,api/workflow,api/tpa等开头的api 
```js  
    ['/api/lcode']: {//低代码平台
      target: 'http://localhost:7014',
      ws: false,
      changeOrigin: true,
      rewrite: (path) => path.replace(new RegExp(`^/api/lcode`), '/'),
    },
    ['/api/oauth2client']: {//登录相关,如果VITE_CTX_LOGIN=lcode,此代理不需要配置，走的是lcode的代理
      target: 'localhost:7014',//如果对接统一认证中心，此处应该改为oauth2client的端口，默认对接lcode
      ws: false,
      changeOrigin: true,
      rewrite: (path) => path.replace(new RegExp(`^/api/oauth2client`), '/'),
    },
    ['/api/arc']: {// 图片、文件、标签、分类等,如果VITE_CTX_ARC=lcode,此代理不需要配置，走的是lcode的代理
      target: 'localhost:7014',//如果对接arc-backend，此处应该改为arc的端口，默认对接lcode
      ws: false,
      changeOrigin: true,
      rewrite: (path) => path.replace(new RegExp(`^/api/arc`), '/'),
    },
    ['/api/workflow']: {//工作流
      target: 'localhost:7080',
      ws: false,
      changeOrigin: true,
      rewrite: (path) => path.replace(new RegExp(`^/api/workflow`), '/'),
    },
    ['/api/tpa']: {//第三方微信、支付宝登录，支付，二维码等
      target: 'localhost:7012',
      ws: false,
      changeOrigin: true,
      rewrite: (path) => path.replace(new RegExp(`^/api/tpa`), '/'),
    },
    ['/api']: {//保底代理
      target: 'http://localhost:7014', //不匹配的请求，全部转向lcode
      ws: false,
      changeOrigin: true,
      rewrite: (path) => path.replace(new RegExp(`^/api`), '/'),
    }, 
```
上述代理实现把以/api/*开头的请求地址(举例/api/lcode/user/list)替换为/user/list,并转发到  http://服务器上的ip:端口 地址  

### 构建

```bash
# 构建测试环境
pnpm run build:test

# 构建生产环境
pnpm run build:prod
```

### 发布
假设：  
1. 应用服务器(sit1-backend),ip为192.168.0.222,端口7014
2. 静态资源服务器(sit1-static),ip为192.168.0.111
3. 反向代理使用nginx
4. 静态资源部署到服务器sit1-static的/data/nginx/html/lcode/下
5. 本地*ui-web目录在C:/Users/xxx/IdeaProjects/mdp-lcode-ui-web/

#### 发布静态资源到测试\生产环境

```bash
# 1.首先登录sit1-static并进入/data/nginx/html/lcode/下

cd /data/nginx/html/lcode/
# 如果有dist.rar需要先删除
rm -rf dist.rar


# 2.执行rz命令，弹框选择本地dist文件上传到sit1-static服务器
rz 

# 3.在弹出的目录选择器中进入C:\Users\xxx\IdeaProjects\mdp-lcode-ui-web\dist\目录下
全选所有文件->右键->添加到dist.rar->选中dist.rar->上传到服务器商/data/nginx/html/lcode/目录下

# 4.执行解压缩命令，至此静态资源发布完成
unrar x dist.rar
解压后m1目录下一般有css、fonts、images、img、js、tinymce、favicon.icon、index.html等文件或者文件夹
```

#### nginx的配置
##### 静态资源的访问映射
```
   location ~*/(mk|mallm|arc|im|sms|workflow|oa|ac|audit|xm|tpa|crowd|lcode) {
    root /usr/share/nginx/html;
    index index.html;
    expires 30d;
  }

```

##### 后端相关api的映射配置(单体应用方式部署的后端)
api/xm, api/lcode, api/arc, api/workflow, api/tpa,等api配置，每个都是同样的逻辑
```
  # lcode 低代码相关api
    upstream api-gate-lcode {
        server 192.168.0.222:7014;
    }

  location /api/lcode/ {
    #proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://api-gate-lcode/;
    proxy_connect_timeout 65s;
    proxy_send_timeout 65s;
    proxy_read_timeout 65s;
    proxy_next_upstream_tries 1;
  }

  # workflow 工作流相关api
    upstream api-gate-workflow {
        server 192.168.0.222:7080;
    }

  location /api/workflow/ {
    #proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://api-gate-workflow/;
    proxy_connect_timeout 65s;
    proxy_send_timeout 65s;
    proxy_read_timeout 65s;
    proxy_next_upstream_tries 1;
  }

  # arc 内容相关api
   upstream api-gate-arc {
    # 如果由lcode提供服务，可以指向lcode所在端口7014
        server 192.168.0.222:7012;
    }

  location /api/arc/ {
    #proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://api-gate-arc/; 
    proxy_connect_timeout 65s;
    proxy_send_timeout 65s;
    proxy_read_timeout 65s;
    proxy_next_upstream_tries 1;
  }
  
  # xm 项目管理相关api
   upstream api-gate-xm {
        server 192.168.0.222:7067;
    }

  location /api/xm/ {
    #proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://api-gate-xm/;
    proxy_connect_timeout 65s;
    proxy_send_timeout 65s;
    proxy_read_timeout 65s;
    proxy_next_upstream_tries 1;
  } 

  # oauth2client 登录相关api
  upstream api-gate-oauth2client {
    # 如果由lcode提供服务，可以指向lcode所在端口7014
      server 192.168.0.222:7002;
  }

  location /api/oauth2client/ {
    #proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://api-gate-oauth2client/;
    proxy_connect_timeout 65s;
    proxy_send_timeout 65s;
    proxy_read_timeout 65s;
    proxy_next_upstream_tries 1;
  }

  
  # tpa 第三方微信/支付宝登录、支付等相关api
  upstream api-gate-tpa { 
      server 192.168.0.222:7125;
  }

  location /api/tpa/ {
    #proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://api-gate-tpa/;
    proxy_connect_timeout 65s;
    proxy_send_timeout 65s;
    proxy_read_timeout 65s;
    proxy_next_upstream_tries 1;
  }
``` 


##### api的映射配置(cloud方式部署的后端)
假设 spring cloud gate部署在192.168.0.222，监听6101端口
下面的目标服务器改为cloud gate的地址端口.
注意cloud环境下不需要像单体般每个子系统api单独配置映射关系。因为所有的api都是经过gate再转发的
```
    upstream api-gate {
        server 192.168.0.222:6101;
    } 
    location /api/ {
      #proxy_redirect off;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://api-gate/;
      proxy_connect_timeout 65s;
      proxy_send_timeout 65s;
      proxy_read_timeout 65s;
      proxy_next_upstream_tries 1;
  }
```

#### 发布后端服务
具体以[mdp-lcode-backend](https://gitee.com/maimengcloud/mdp-lcode-backend)中发布描述为准

### 运维
#### 日志查询
日常查询问题，主要通过查询前后端日志，分析出具体原因，再进行针对性解决。
唛盟平台具有多个子业务系统，每个子业务系统都有独立日志，一个前端请求，可能日志散落在多个日志文件，如何才能凑齐这些日志，进行统一分析交易执行情况，进而定位问题？  
答复：唛盟平台在每个请求到达服务端的时候，会检测gloNo是否存在，如果不存在，则会自动创建一个gloNo,并注入到日志上下文中，并打印到每一行代码中，另外请求结束，还会把gloNo返回到前端，如果交易中间涉及调用其他子系统，同样会自动上送gloNo，对方交易子系统同样执行上诉逻辑。因此，可以看见，gloNo是能够把所有日志串联起来的。我们只需要在日志文件目录下执行grep命令即可找出对应日志  
操作步骤：  
1.下面是典型的唛盟接口返回的数据结果集 gloNo就存放在res.data.tips.gloNo中。 
```
{
  "total" : 1,
  "data" : [ {
    "bizFlowState" : null,
    "deptName" : null,
    "projectCnt" : 1,
    "admUserid" : "4hinb8m16",
    "del" : "0",
    "budgetNouserAt" : null,
    "productName" : "唛盟云-财务系统",
    "baselineId" : "2022-01-01-base-01", 
  }],
  "tips" : {
    "msg" : "成功",
    "gloNo" : "20240312200013556UWJZKV",
    "isOk" : true,
    "reqNo" : "20240312200013556JM3WKN",
    "tipscode" : "ok"
  }
}
```
2. 通过gloNo到logs目录下执行

```
grep "20240312200013556UWJZKV" *.log
```

#### 服务状态监控
consule 的默认控制台
http://ip:8500/

#### 有时候微服务需要人为干预进行下架
可以调用[/bin/service-deregister.sh](https://gitee.com/maimengcloud/mdp-lcode-backend/blob/master/bin/service-deregister.sh)进行

#### 日志定时备份清理
可以在crontab中配置定时任务，定时调用[bin/clear-log.sh](https://gitee.com/maimengcloud/mdp-lcode-backend/blob/master/bin/clear-log.sh)即可完成7天循环自动清理及备份日志

使用举例： 
```bash
# 打开定时任务编辑器
crontab -e  
# 查看定时任务
crontab -l 
# 增加定时清理备份日志任务
0 1 * * * /bin/bash /home/mall/bin/clear-log.sh

```


### 🔔️ 开发计划与进度情况 

#### 3.x.x (当前版本，已完成大部分内容) 

1. 后端底座提供el表达式沙箱环境，支持前端书写表达式上送后端，为0代码，ai的发展提供基础能力！
2. 后端提供无服务，无dao，无实体，无控制层的四无crud通用接口，为实现应用开发部署发布0耗时提供底座能力！
3. 提供数据模型子系统，为在线浏览数据，操作数据实体，构建数据集市提供底座基础！提供前端sql执行、sql结果动态展示等，支持在线修改表结构、在线预览结构信息、在线预览修改表数据等ddl、dml能力
4. 进一步对ai编程提供一些底座能力
5. 提供新版智能表单前后端，支持0代码设计、开发、发布一个表单功能(基于formCreate进行魔改)，支持0耗时免部署
6. 提供新版工作流编辑器
7. 提供智能大屏，智能图表在线构造发布演示基础框架
8. 提供任意接口任意字段起调ai，java，计算公式等能力 
9. 进一步整合lcode,将基础内容管理、数据模型、工作流、系统管理、应用超市(下单、购买、订单管理全套)整合进lcode
10. 提供docker版本，免编译快速部署（方便非it人士使用）
11. 提供自研单机版内存框架，本地跑不需要集成redis等第三方内存框架，提供redis的适配包，默认打包进bootstrap工程。上线内存管理自动切换到redis
12. 重构mdp-ui、mdp-ui-ext各个组件
13. 前端公共配置项与VITE公共配置项整合
14. 提供内置的图标浏览、选用功能
15. 提供图片库浏览、选择图片、管理图片等功能
16. 提供附件库浏览、选用、管理等功能
17. 提供路由配置导入菜单表功能。（免去手工增删改查菜单的麻烦，升级改造调整菜单不再困难）
18. 每个子系统提供单体版、cloud版、oauth2版，版本之间免改代码自由切换。（免去选择困难症）
19. 字典功能、平台配置功能提供配置表动态配置功能，无须改改动表结构即可任意添加配置项，彻底告别加配置犯难症
20. 字典配置、平台配置支持全面覆盖，无论在ui页面、java后端等提供自动加载、自动分布式分发、同步机制，彻底告别性能忧虑、前后端编码冲突打架的问题
   
#### 4.x.x (下一版本，让我们一起拥抱ai起飞吧)
1. 提供接入大模型能力
2. 提供前端任意字段起调ai能力
3. 提供ai编程、ai测试、ai编排任务、ai需求整理能力
4. 提供ai出报告能力
5. 提供ai基于企业私有数字资产自训练能力

### 🗒️ 版本更新日志

#### 2.0.0-RELEASE 
  已发布，停更
#### 3.0.0-RELEASE
 已发布，持续改进

#### 4.0.0-BETA
  预研中 


## 📝 常见问题  

#### 本地前后端对接不上  
答复：  
1.请排查[vite.config.ts](vite.config.ts)中的代理是否配置正确，详细配置说明参考前面关于前后端对接的说明；  
2.请检查后端服务是否已启动，并查看配置文件关于服务监听端口是否与代理配置的端口一致；

#### 前端pnpm install出现各种编译错误
答复：  
1.请排查node的版本是否是前面描述的指定版本；  
2.请使用nvm进行node版本管理，具体请看 [如何切换node.js版本](#如何切换nodejs版本)  

#### 前端pnpm install时，下载缓慢如何解决
答复：设置国内镜像 
```js
//查看源
pnpm config get registry 
//切换淘宝源
pnpm config set registry https://registry.npmmirror.com/
``` 
#### 默认登录账户是什么？
答复：账户为: demo-branch-01 密码为: 888888
演示环境大家切勿自行改密码

#### 超级管理员账户是什么?
答复： 账户为: superAdmin 密码为: 888888
演示环境大家切勿自行改密码

#### 数据库脚本在哪？
答复: 数据库脚本一般存放在后端工程下的sql/文件夹下
低代码的[mdp-lcode-backend/mdp-lcode/sql/lcode.sql](https://gitee.com/maimengcloud/mdp-lcode-backend/tree/master/mdp-lcode/sql)

#### 数据库的编码是什么？
mysql的话，建议character set: utf8mb4, collation: utf8mb4_croatian_ci
mysql8以上

#### 为什么后端下载下来cloud的相关包报错
答复：因为本地跑不需要cloud环境，cloud相关包是部署到cloud环境才需要的。您可以采取以下两种办法之一解决  
3.0以上版本不存在该问题，3.0以下版本按下面办法处理
办法1. 找到根目录下的pom.xm文件，把cloud的模块注释掉
```xml
	<modules>
		<module>mdp-arc-mate</module>
		<module>mdp-arc-pub</module>
		<module>mdp-sys</module>
		<module>mdp-lcode</module>
		<module>mdp-lcode-bootstrap</module>
		<!-- <module>mdp-lcode-cloud-bootstrap</module> -->
	</modules>
```
办法2. 下载[mdp-cloud-backend](https://gitee.com/maimengcloud/mdp-cloud-backend)工程，进行安装

#### 我只需要研发管理系统，需要看低代码平台么？
答复：需要的，原因：1.研发管理系统采用低代码中的框架开发，各种组件的使用说明，框架的说明介绍等都是放在低代码平台中。2.研发管理系统中的账户、角色权限、组织、字典、菜单等都是直接采用低代码平台的功能，研发管理系统必须结合低代码平台才能算是一个完整的产品。


#### 如何切换node.js版本
答复：请参考以下文章[nvm介绍、安装、报错处理以及使用](https://blog.csdn.net/weixin_46864830/article/details/124621343)  
注意：请用管理员身份打开cmd命令窗口  
1.查看本地安装所有版本以及当前使用的版本，有可选参数available，显示所有可下载的版本
```js
nvm list [available]
```
2.查看到 node 版本后，推荐使用 LTS 中任意一个版本进行安装，命令中版本自定义，例如下
```js
nvm install 21.1.0
```
3.使用 nvm 进行切换 node 版本使用特定版本
```js
nvm use 21.1.0
```

#### 为什么唛盟的工程如此分散？
答复：唛盟属于企业级开发平台，企业可以用唛盟开发任何业务系统，在企业内部来讲，最终可能演变成1底座+n个专业子系统的最终形态。唛盟采用1+n架构正是由目前大部分企业的这种形态决定的，唛盟为适应1+n的架构做了最佳的工程划分实践，在很多项目实战中获得了巨大成功。1代表核心底座mdp-core,地位非常重要，任何改动有可能影响所有产品的正确运行，一般由架构师团队维护，不建议开放给各个业务组，所以mdp-core有完全独立开来的需要。n代表各个子系统，子系统之间高内聚，低耦合。每个子系统完全可以独立运行的。在管理上，可能每个子系统属于不同的产品团队或者不同的项目团队进行管理。工程包完全独立，方便管理，避免权责不清，互相交叉等问题。

#### 我只需要某个专业子系统，我能把各个分散的工程合并起来么？
答复：可以的，唛盟设计之初就考虑到了需要具备可合可分的这种灵活性。唛盟采用1+n架构，具体体现为至少有n个jar包，jar包是可以互相引入的，可以在-bootstrap的工程中通过pom引入的方式把他们合并成一个大的发布包。最终发布到服务器的时候，发布一个jar包就可以了。

### 登录成功后报登录超时，请重新登录
答复：登录成功后，会对统一流程中心发起一次代办查询，如果未部署流程中心，将代办的请求发送到唛盟官网。由于开发者本地跑产生的token是由lcode颁发，对唛盟官网的请求带上的token将被唛盟官网鉴权不通过，报401.开发者需要检查以下配置：  
1. [vite.config.ts proxy](vite.config.ts)是否配置了默认对唛盟官网请求的保底代理，如有，请删除。 
2. 如果不需要工作流，将VITE_WF_OPEN设为false,将不再发起代办的请求  
以下是需要工作流的情况：  

3. 如果需要工作流，请检查[vite.config.ts proxy](vite.config.ts)是否配置了对流程中心的请求代理，如果未配置，请查阅[流程整合](#流程编辑器的整合)
4. 统一流程中心启动配置项是否将 mdp.jwt.connect-oauth2-server配置为false?
```properties
mdp:
  jwt:
    connect-oauth2-server: false
```
5. 统一流程中心中心的java工程启动项配置是不是未成功?（大部分初学者都是改了配置不生效，属于java工程未正确启动或者未正确编译）

以上自检查确认后未解决问题，打开浏览器控制台截取交易报送信息及返回信息，联系唛盟客服。

### 本地跑单体需要redis吗？ 
答复：唛盟底座架构及各子系统默认完全剥离redis，采用自研的CacheHKV缓存框架。如果不需要分布式缓程，理论上不需要引入任何第三方缓存框架。但是CacheHKV设计并不是要真正做缓存框架，而是为了开发调试方便，上生产的时候，建议还是引入真正的缓存框架。  
### 引入redis缓存涉不涉及改代码？ 
答复：
1. 如果CacheHKV提供了足够的api，不涉及改代码，只需要在*-bootstrap打包工程中的pom.xml文件引入唛盟为redis开发的适配包即可。
```properties 
  <dependency>
      <groupId>com.mdp</groupId>
      <artifactId>mdp-boot-starter-redis</artifactId>
  </dependency>
```
2. 如果CacheHKV提供的api无法满足需求，需要使用redis等第三方框架侵入式到代码中，可以在业务工程中的pom.xml引入mdp-boot-starter-redis,可以像普通的用法一样，绕过CacheHKV，直接使用redis的api.

### CacheHKV的api能根redis的api混用吗？
答复：可以混用，你可以用CacheHKV put值，通过redis get值，同理，使用redis put值，通过CacheHKV取值也是一样的

### 菜单如何授权？
答复： 要回答这个问题，首先要理解唛盟菜单的机制。
#### 唛盟菜单机制说明：  
1. 唛盟菜单是不需要手动创建的，唛盟生态每个子系统内置了菜单自动导入功能，用超级管理登录即可看见
2. 唛盟菜单同时支持前端配置菜单、后端配置菜单，两者可以混用，唛盟会进行合并形成最终的展示菜单
3. 当然唛盟也同时支持前端授权、后端授权，两者可以混用，唛盟也会进行交叉授权检查，形成最终展示菜单
#### 什么是前端授权
答复： 前端授权即后台不配制任何菜单，前端也可以按自己的菜单授权逻辑控制菜单的显示也否。
#### 前端授权的逻辑是怎么样的？
答复： 前端在路由meta.roles配置角色列表，告诉唛盟平台该菜单属于前端授权菜单。需要进行角色判断才能展示
唛盟将会获取当前登录用户的角色列表与菜单上配置的角色列表进行匹配，匹配到任何一个即放行
```js
{
  meta:{
    roles:['superAdmin','hrrs']
  }
}
```
#### 什么是后端授权？
答复：后端授权指通过后台配置，将菜单按角色分配，达到控制不同角色能看到的菜单不同的目的
前端框架底层将会按后端返回的菜单做以下操作：
  1. 前端带有meta.menu=true的路由，如果后端没有返回同名路由，说明该前端路由必须隐藏
  2. 前端带有meta.menu=true的路由，如后端返回有同名路由，保留前端，剔除后端
  3. 前端没有meta.menu的路由，默认不做处理，如果后端返回同名，则将后端剔除
  4. 如果后端返回一个前端没有配置的路由，则需要添加到前端路由中
#### 后端授权需要前端开发做什么？
答复：后端授权需要前端在路由配置中设置 meta.menu=true
```js
{
  meta:{
    menu:true
  }
}
```
#### 前端授权、后端授权同时配置会如何？
答复： 如果一个菜单既配置了meta.menu=true,同时配置了meta.roles，那么唛盟底层将以后端授权为准，忽略前端授权
#### 超级管理员的授权问题
答复：  超级管理员无视任何授权规则，一律放行 

#### 后端动态菜单返回了其它子系统的菜单，如何解决？
 答复： 唛盟生态具有多个子系统，每个子系统都是可以导入各自菜单的，因此，势必造成后端菜单扎堆，那么如何进行菜单范围限制呢？  
1. 在前端配置全局配置项。设定本应用的模块范围，在设定范围内的动态菜单才允许放行，否则忽略

2. 如果全局配置为空，则默认全部放行
```js
VITE_MODULE_SCOPE=['模块编号1','模块编号2']
```

## 💯 实践案例

1. [低代码平台](https://maimengcloud.com/lcode)
1. [系统管理](https://maimengcloud.com/sys)
2. [协同办公](https://maimengcloud.com/oa)
3. [唛盟众包-网页](https://maimengcloud.com) 
3. [项目管理-网页](https://maimengcloud.com/xm) 
4. 项目管理-小程序   
   <img src="./docs/images/index/mm_wxapp_qrcode.png" alt="drawing" width="200"/>
5. [流程管理](https://maimengcloud.com/lcode/)
 
### 项目分支说明

| 分支     | 说明                                            |
|--------|-----------------------------------------------|
| master | 主分支，3.0.0+,受保护分支，此分支不接受 PR。在 dev 分支后经过测试没问题后会合并到此分支。 |
| 3.0.0-RELEASE    | 已发布的版本3.0。                    | 
| 2.0.0-RELEASE    | 已发布的版本2.0。                    |

## 🐞 交流讨论 、反馈 BUG、提出建议等

1. 快扫描下方左侧微信二维码和我们一起交流讨论吧！（备注 唛盟-mdp 进群） 
<img src="./docs/images/index/mm_wx_kf_qrcode.png" alt="drawing" width="200"/>

2. 唛盟微信公众号查看一些基础教程  
<img src="./docs/images/index/mm_wxpub_qrcode.png" alt="drawing" width="200"/>

3. 反馈 BUG、提出建议，欢迎新建：[issues](https://gitee.com/maimengcloud/mdp-lcode-ui-web/issues)，开发人员会不定时查看回复。
4. 参与贡献，请查看[贡献指南](#🔨贡献指南)。

## 💲 打赏
 **感谢所有赞赏以及参与贡献的小伙伴，你们的支持是我们不断更新前进的动力！微信扫一扫，赏杯咖啡呗！**    
 <img src="./docs/images/index/mm_pay_coffee.jpg" alt="drawing" width="300"/>

## 🔔商务合作 

序号|合作项目| 详细介绍 | 费用说明|
----------------------|------------|----------------------|-----|
1.| 打赏获得赞助商名额| 在赞助商列表展示（添加微信沟通） |不限额度|
2.| 新组件开发| 提供组件扩展、优化服务 |视复杂程度而定|
3.| 开发问题解答|如果使用该产品遇到棘手问题需要解决，添加微信进行沟通解决 |免费|
4.| 开发培训|提供开发流程介绍、技术介绍、功能介绍、部署流程介绍，仅限线上培训 |加微信详聊|
5.| 扩展问题解答|如果需要使用该产品进行自有业务系统研发，需要我方提供意见建议，我方收取一定费用后提供相应服务 |加微信详聊|
6.| 广告合作|广告位(精品项目推荐、赞助商展位) |加微信沟通|




## 📢 视频教程(2.**的视频教程)   

[0.1-唛盟-唛盟生态1+n架构无限扩展、丝滑部署、永不下线](https://www.bilibili.com/video/BV1BD421V7Bu/?spm_id_from=333.788.recommend_more_video.1&vd_source=93be23d03863773d50b81112985b9237)  

[1.唛盟-总体介绍](https://www.bilibili.com/video/BV111421S72r/?vd_source=93be23d03863773d50b81112985b9237)  

[2.唛盟-工程结构介绍](https://www.bilibili.com/video/BV1Nu4m1T7hc/?spm_id_from=333.788.recommend_more_video.0&vd_source=93be23d03863773d50b81112985b9237)  

[3.唛盟-工程结构-后端内部结构介绍](https://www.bilibili.com/video/BV1Lj421d7Xb/?spm_id_from=333.788.recommend_more_video.3&vd_source=93be23d03863773d50b81112985b9237)  

[4.唛盟-工程结构-前端内部结构介绍](https://www.bilibili.com/video/BV1ut421V7un/?spm_id_from=333.788.recommend_more_video.2&vd_source=93be23d03863773d50b81112985b9237)  

[5.唛盟-智能表单-代码生成等模块介绍](https://www.bilibili.com/video/BV1UH4y1p7uJ/?spm_id_from=333.788.recommend_more_video.16&vd_source=93be23d03863773d50b81112985b9237)  

[5.0-唛盟-前后端开发新手攻略](https://www.bilibili.com/video/BV1aC411t7rG/?vd_source=93be23d03863773d50b81112985b9237)

[6.唛盟-用户、权限、菜单、组织管理等模块介绍](https://www.bilibili.com/video/BV18m411k7rq/?spm_id_from=333.788.recommend_more_video.5&vd_source=93be23d03863773d50b81112985b9237)  

[7.唛盟-前端工程开发、部署流程介绍](https://www.bilibili.com/video/BV1ut421G7ec/?spm_id_from=333.788.recommend_more_video.5&vd_source=93be23d03863773d50b81112985b9237)  

[8.唛盟-后端工程开发、部署流程介绍](https://www.bilibili.com/video/BV1hx421k7tm/?spm_id_from=333.788.recommend_more_video.5&vd_source=93be23d03863773d50b81112985b9237)  

[8.1.唛盟-后端工程开发-mdp-core介绍](https://www.bilibili.com/video/BV1EC41187jE/?spm_id_from=333.788.recommend_more_video.8&vd_source=93be23d03863773d50b81112985b9237)  

[8.2.唛盟-后端工程开发-控制层、服务层、dao层开发详细介绍](https://www.bilibili.com/video/BV1vu4m1T7V6/?spm_id_from=333.788.recommend_more_video.7&vd_source=93be23d03863773d50b81112985b9237)  

[8.3.唛盟-如何部署单体应用](https://www.bilibili.com/video/BV1df421d75w/?spm_id_from=333.788.recommend_more_video.2&vd_source=93be23d03863773d50b81112985b9237)  

[9.1.唛盟-本地开发前后端如何对接的](https://www.bilibili.com/video/BV1Ym421n7Vh/?vd_source=93be23d03863773d50b81112985b9237)  

[9.2.唛盟-前端编译报错如何解决](https://www.bilibili.com/video/BV1PC411b7qY/?spm_id_from=333.788.recommend_more_video.6&vd_source=93be23d03863773d50b81112985b9237)  

[9.4.唛盟-日志规范及如何快速定位问题](https://www.bilibili.com/video/BV1Hm421n7Qv/?spm_id_from=333.788.recommend_more_video.2&vd_source=93be23d03863773d50b81112985b9237)  

[10-唛盟-超强查询接口实现方案、原理解析](https://www.bilibili.com/video/BV1Ct421g72j/?vd_source=93be23d03863773d50b81112985b9237)  

[10.1-唛盟-复杂查询、多主键crud的实现方案、原理解析](https://www.bilibili.com/video/BV1KH4y1H7eM/?vd_source=93be23d03863773d50b81112985b9237)

[11-唛盟-超强批量更新接口实现方案、原理解析](https://www.bilibili.com/video/BV1V1421D7z6/?vd_source=93be23d03863773d50b81112985b9237)  

[12-唛盟-多数据源的实现方案、常见问题解决办法解析](https://www.bilibili.com/video/BV1Zp421y7ty/?vd_source=93be23d03863773d50b81112985b9237)

[13-唛盟-后端国际化多语言版的实现方案、原理解析](https://www.bilibili.com/video/BV1at421n7JW/?spm_id_from=333.337.search-card.all.click&vd_source=93be23d03863773d50b81112985b9237)

[14-唛盟-流程整合、原理解析](https://www.bilibili.com/video/BV13x4y1v7qX/?vd_source=93be23d03863773d50b81112985b9237)

[15-唛盟-分布式数据字典的实现方案、使用示例详解](https://www.bilibili.com/video/BV1hE421g7FT/?spm_id_from=333.788&vd_source=93be23d03863773d50b81112985b9237)


## 📢工程介绍
👉[唛盟-工程结构介绍](https://www.bilibili.com/video/BV1Nu4m1T7hc/?spm_id_from=333.337.search-card.all.click&vd_source=93be23d03863773d50b81112985b9237)
>⚠️工程分类：主体工程、基础配套工程、高级配套工程、拓展应用工程、辅助开发工具五大类。  
> 主体工程：本系统业务工程，用于书写本系统相关的业务代码；  
> 基础配套工程：业务工程运行所需的必要工程；如开发底座，系统管理相关功能包  
> 高级配套工程：高级应用，比如需要将业务工程接入oauth2环境、将业务工程发布到cloud环境，才需要下载安装高级配套工程   
> 拓展应用工程： 基于唛盟低代码平台拓展的应用工程，其后端可以单体发布，也可以以微服务方式发布，也可以打成jar被唛盟其它应用引入   
> 辅助开发工具：指开发用到的一些工具  

>⚠️工程命名规范：业务名称+工程属性  
> 工程属性如下：  
> -ui: 页面工程  
> -web: pc端运行的浏览器工程  
> -backend: 后端服务  
> -app: 安卓、ios等原生app工程  
> -uniapp: 基于uniapp开发的应用  
> -minapp: 小程序   
> -cloud: 微服务cloud环境  
> -oauth2: 统一认证中心   
> -bootstrap: 打包部署工程

### 唛盟主体工程
唛盟属于前后端严格分离的一套系统，分为前端工程、后端工程两部分
- [mdp-lcode-ui-web](https://gitee.com/maimengcloud/mdp-lcode-ui-web)
唛盟的前端工程,以vue.js为主
- [mdp-lcode-backend](https://gitee.com/maimengcloud/mdp-lcode-backend)
唛盟的后端工程，java语言为主  

### 基础配套工程

- [mdp-core 启动后端的话必须先安装此工程](https://gitee.com/maimengcloud/mdp-core)   
  该工程作为所有后端的公共封装，不包含任何的业务功能，仅作为后端系统的技术底座，包含一些公共工具、服务类抽象、dao层抽象等。
  
### 高级配套工程
- [mdp-cloud-backend 非必须](https://gitee.com/maimengcloud/mdp-cloud-backend)   
  该工程作为spring cloud的扩展工程，如果需要将系统发布到spring cloud环境，需要下载该工程重新打包，不包含任何的业务功能，仅作为后端接入cloud环境使用

- [mdp-oauth2-backend 非必须](https://gitee.com/maimengcloud/mdp-oauth2-backend)   
  该工程为统一认证中心，作为spring oauth2的扩展工程，如果需要将系统接入oauth2环境，需要下载该工程重新打包，该工程实现分布式单点登陆；支持微信、支付宝、短信、账户密码等多种登陆方式

### 开发辅助工具
- [mdp-code-generator 非必须](https://gitee.com/maimengcloud/mdp-code-generator) 代码生成器  
  生成代码两种方式，  
  一、使用代码生成器本地工程，好处是代码可以直接覆盖到本地业务工程，避免复制黏贴出错。  
  二、如果不需要本地生成代码，也可以在低代码平台在线生成。[在线生成代码](https://maimengcloud.com/lcode/#/mdp/lcode/gen)

### 拓展应用工程
- 拓展应用工程： 基于唛盟低代码平台拓展的应用工程，其后端可以单体发布，也可以以微服务方式发布，也可以打成jar被唛盟其它应用引入   