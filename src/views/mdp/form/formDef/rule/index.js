
import MdpSelect from './mdpSelect'; 
import MdpSelectMeta from './mdpSelectMeta';   
const MdpRule = {
    install: function(designer){
 

        //插入组件规则
       designer.addComponent(MdpSelect); 
       designer.addComponent(MdpSelectMeta); 

        //自定义左边按钮
        
       designer.appendMenuItem({
            icon: 'icon-select',
            name: 'MdpSelect',
            label: '下拉列表'
        });

        designer.appendMenuItem({
            icon: 'icon-select',
            name: 'MdpSelectMeta',
            label: '选择字典'
        });

       designer.addMenu({
        title:'唛盟扩展组件',
        name:'mdp',
        list:[
            {
                icon: 'icon-select',
                name: 'MdpSelect',
                label: '列表'
            },
            {
                icon: 'icon-select',
                name: 'MdpSelectMeta',
                label: '数据字典'
            },
            
            // {
            //     icon: 'icon-checkbox',
            //     name: 'MdpSelect',
            //     label: '多选框'
            // },
            
            // {
            //     icon: 'icon-radio',
            //     name: 'MdpSelect',
            //     label: '单选框'
            // }
        ]
        });
    }
}
export default MdpRule