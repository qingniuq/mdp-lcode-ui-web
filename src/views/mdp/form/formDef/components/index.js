

import MdpSelectDataSource from './select/DataSource.vue'
import MdpInputTableName from './input/TableName.vue'
import MdpInputDataSet from './input/DataSet.vue'
import MdpInputTableField from './input/TableField.vue'
import MdpInputUiField from './input/UiField.vue'
import MdpCrudApisCfg from './input/CrudApisCfg.vue'
const MdpDesigner = {
    install: function(Vue){  
        Vue.component('MdpSelectDataSource', MdpSelectDataSource) 
        Vue.component('MdpInputTableName', MdpInputTableName) 
        Vue.component('MdpInputTableField', MdpInputTableField) 
        Vue.component('MdpInputUiField', MdpInputUiField) 
        Vue.component('MdpCrudApisCfg', MdpCrudApisCfg) 
        Vue.component('MdpInputDataSet', MdpInputDataSet) 
    }
}
export default MdpDesigner