/* Layout */
import { Layout } from '@/utils/routerHelper'
const { t } = useI18n()

const RoutesLcode: AppRouteRecordRaw[] = [ 
  {
    path: '/mdp/larc',
    component: Layout,
    name: 'larc', 
    meta: {
      title: t('router.larc.index'),
      icon: 'fa-solid:atom'
    },
    //leaf: true,//只有一个节点
    children: [
      {
        path: 'att',
        component: () => import('@/views/mdp/larc/att/Index.vue'),
        name: 'larcAtt',
        meta: { title: t('router.larc.att'), icon: 'fa-solid:file',noCache:false ,
        menu: true},
        keepAlive: true
      },
      {
        path: 'image',
        component: () => import('@/views/mdp/larc/image/Index.vue'),
        name: 'larcImage',
        meta: { title: t('router.larc.image'), icon: 'fa-solid:images',noCache:false,
        menu: true },
        keepAlive: true
      },
      {
        path: 'tag',
        component: () => import('@/views/mdp/larc/tag/Index.vue'),
        name: 'larcTag',
        meta: { title: t('router.larc.tag'), icon: 'fa-solid:dice-d20',noCache:false,
        menu: true },
        keepAlive: true
      },
      {
        path: 'icon',
        component: () => import('@/components/Icon/src/IconViewer.vue'),
        name: 'larcIcon',
        meta: { title: t('router.larc.icon'), icon: 'fa-solid:atom',noCache:false,
        menu: true },
        keepAlive: true
      }
    ]
  }
]

export default RoutesLcode
