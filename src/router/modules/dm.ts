/* Layout */
import { Layout } from '@/utils/routerHelper'
const { t } = useI18n()

const RoutesDm: AppRouteRecordRaw[] = [
  {
    path: '/mdp/dm',
    component: Layout,
    name: 'dmDataModel', 
    meta: {
      title: t('router.dm.dataModel'),
      icon: 'ep:data-line'
    },
    //leaf: true,//只有一个节点
    children: [
      { 
        path: 'model/index',
        component: () => import('@/views/mdp/dm/model/Index.vue'),
        name: 'dmModelIndex',
        meta: {
          title: t('router.dm.modelIndex'),
          menu: true
        }
      },
      { 
        path: 'meta/index',
        component: () => import('@/views/mdp/dm/meta/Index.vue'),
        name: 'dmTableIndex',
        meta: {
          title: t('router.dm.tableIndex'),
          menu: true
        }
      },
      { 
        path: 'meta/table/struct/:dataSource/:tableName',
        component: () => import('@/views/mdp/dm/meta/TableStruct.vue'),
        name: 'dmTableStruct',
        meta: {
          title: t('router.dm.TableStruct'),hidden:true,
          menu: true
        }
      }, 
      { 
        path: 'data/table',
        component: () => import('@/views/mdp/dm/data/Index.vue'),
        name: 'dmDataTable',
        meta: {
          title: t('router.dm.dataTable'),
          menu: true
        },
      },
      { 
        path: 'data/list/:dataSource/:tableName',
        component: () => import('@/views/mdp/dm/data/TableDataIndex.vue'),
        name: 'dmTableDataList',
        meta: {
          title: t('router.dm.tableDataList'),
          menu: true,
          hidden: true
        },
      },
      { 
        path: 'data/set',
        component: () => import('@/views/mdp/dm/dataSet/Index.vue'),
        name: 'dmDataSetIndex',
        meta: {
          title: t('router.dm.dataSet'),
          menu: true
        },
      },
      { 
        path: 'data/set/edit/:id',
        component: () => import('@/views/mdp/dm/dataSet/Form.vue'),
        name: 'dmDataSetForm',
        meta: {
          title: t('router.dm.dataSet'),
          hidden: true,
          menu: true
        },
      },
      { 
        path: 'data/set/list/:id',
        component: () => import('@/views/mdp/dm/dataSet/IndexDataList.vue'),
        name: 'dmDataSetDataList',
        meta: {
          title: t('router.dm.dataSetDataList'),
          hidden: true,
          menu: true
        },
      },
      { 
        path: 'data/set/create',
        component: () => import('@/views/mdp/dm/dataSet/Form.vue'),
        name: 'dmDataSetCreate',
        meta: {
          title: t('router.dm.dataSetCreate'), 
          menu: true
        },
      }
    ]
  }
]

export default RoutesDm
