import axios from '@/config/maxios'

 import config from '@/api/mdp_pub/mdp_config'

let base = config.getLcodeCtx();
 
//在线生成crud代码并下载
export const crudCodeGen = params => { return axios.get(`${base}/mdp/dev/code/gen`, {params:params}); };


//在线生成crud代码并下载
export const dataSourceList = params => { return axios.get(`${base}/mdp/dev/code/dataSource/list`, {params:params}); };