import axios from '@/config/maxios'

import config from '@/api/mdp_pub/mdp_config'

let base = config.getImBasePath();

export const publishMessage = params => { return axios.post(`${base}/mdp/im/push/publishMessage`, params); };