import axios from '@/config/maxios'
import config from '@/api/mdp_pub/mdp_config'

let base = config.getSysCtx()

export const userMenuFavoriteList = params => { return axios.get(`${base}/menu/menuFavorite/list`, { params: params }); };
export const saveMenuFavoriteList = params => { return axios.post(`${base}/menu/menuFavorite/batchSaveMenuFavorites`, params); };
