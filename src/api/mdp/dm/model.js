import axios from '@/config/maxios'

import config from '@/api/mdp_pub/mdp_config'

let base = config.getDmCtx();

/**
 * 
 * 1 默认只开放普通查询，所有查询，只要上传	 分页参数 {pageNum:当前页码从1开始,pageSize:每页记录数,total:总记录【数如果是0后台会自动计算总记录数非0不会自动计算】}，后台都会自动按分页查询 其它 api用到再打开，没用到的api请注释掉，
 * 2 查询、新增、修改的参数格式  params={id:'模型编号 主键',name:'模型名称',branchId:'归属企业编号',cuserid:'创建人',ctime:'创建时间'}
 * @author maimeng-mdp code-gen
 * @since 2024-5-4
 **/
 
//普通查询 条件之间and关系  
export const listModel = params => { return axios.get(`${base}/mdp/dm/model/list`, { params: params }); };


//普通查询 条件之间and关系  
export const listModelCacheFirst = params => { return axios.get(`${base}/mdp/dm/model/list/cacheFirst`, { params: params }); };

//普通查询 条件之间and关系
export const queryModelById = params => { return axios.get(`${base}/mdp/dm/model/queryById`, { params: params }); };

//删除一条 params={id:'模型编号 主键'}
export const delModel = params => { return axios.post(`${base}/mdp/dm/model/del`,params); };

//批量删除  params=[{id:'模型编号 主键'}]
export const batchAddModel = params => { return axios.post(`${base}/mdp/dm/model/batchAdd`, params); };

//批量删除  params=[{id:'模型编号 主键'}]
export const batchDelModel = params => { return axios.post(`${base}/mdp/dm/model/batchDel`, params); };

//修改一条记录
export const editModel = params => { return axios.post(`${base}/mdp/dm/model/edit`, params); };

//新增一条
export const addModel = params => { return axios.post(`${base}/mdp/dm/model/add`, params); };

//批量修改某些字段
export const editSomeFieldsModel = params => { return axios.post(`${base}/mdp/dm/model/editSomeFields`, params); };